package org.example;

import java.security.SecureRandom;


public class Main {
    public static void main(String[] args) {

        final int linha = 3;
        final int coluna = 3;
        int[][] numeros = new int[linha][coluna];

        for (int l = 0;l < linha; l++){
            for(int c = 0; c < coluna; c++){
                numeros[l][c] = new SecureRandom().nextInt(100);

            }
        }

        for (int l = 0;l < linha; l++){
            for(int c = 0; c < coluna; c++){
                System.out.printf("%2d | ",numeros[l][c]);

            }
            System.out.printf("%n");

        }
        int determinante = numeros[0][0] * numeros[1][1] * numeros[2][2] +
                numeros[0][1] * numeros[1][2] * numeros[2][0] +
                numeros[0][2] * numeros[1][0] * numeros[2][1] -
                numeros[0][2] * numeros[1][1] * numeros[2][0] -
                numeros[0][0] * numeros[1][2] * numeros[2][1] -
                numeros[0][1] * numeros[1][0] * numeros[2][2];

        System.out.printf("%n");
        System.out.println("A Determinante da Matriz é : " + determinante);
    }
}



