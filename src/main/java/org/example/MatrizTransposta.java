package org.example;

import java.security.SecureRandom;

public class MatrizTransposta {
    public static void main(String[] args) {

        final int linha = 3;
        final int coluna = 3;
        int[][] numeros = new int[linha][coluna];

        for (int l = 0; l < linha; l++) {
            for (int c = 0; c < coluna; c++) {
                numeros[l][c] = new SecureRandom().nextInt(100);
            }
        }

        // Imprime a matriz original
        System.out.println("Matriz original:");
        printMatrix(numeros);

        // Calcula e imprime a matriz transposta
        System.out.println("\nMatriz transposta:");
        int[][] transposta = transposeMatrix(numeros);
        printMatrix(transposta);
    }

    // Método para imprimir a matriz
    public static void printMatrix(int[][] matrix) {
        for (int l = 0; l < matrix.length; l++) {
            for (int c = 0; c < matrix[0].length; c++) {
                System.out.printf("%2d | ", matrix[l][c]);
            }
            System.out.println();
        }
    }

    // Método para calcular a matriz transposta
    public static int[][] transposeMatrix(int[][] matrix) {
        int[][] transposta = new int[matrix[0].length][matrix.length];
        for (int l = 0; l < matrix.length; l++) {
            for (int c = 0; c < matrix[0].length; c++) {
                transposta[c][l] = matrix[l][c];
            }
        }
        return transposta;
    }
}
